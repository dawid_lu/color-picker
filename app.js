var app = angular.module("myApp",[]);
	app.controller("MyCtrl",function($scope){
		$scope.red = 0;
		$scope.green = 0;
		$scope.blue = 0;
		var outRed = 0;
		var outGreen = 0;
		var outBlue = 0;
		$scope.setColor = function(colorRed,colorGreen,colorBlue){
		if (colorRed != 0 && colorRed < (outRed+40) && colorRed > (outRed-40)) {
			outRed = colorRed;
			document.getElementById("progressRed").style.width = (outRed*2)+"px";
		}
		else if (colorGreen != 0 && colorGreen < (outGreen+40) && colorGreen > (outGreen-40)) {
			outGreen = colorGreen;
			document.getElementById("progressGreen").style.width = (outGreen*2)+"px";
		}
		else if (colorBlue != 0 && colorBlue < (outBlue+40) && colorBlue > (outBlue-40)) {
			outBlue = colorBlue;
			document.getElementById("progressBlue").style.width = (outBlue*2)+"px";
		}
			document.getElementById("colorWheel").style.backgroundColor = "rgb("+outRed+","+outGreen+","+outBlue+")";
			$scope.red = outRed;
			$scope.green = outGreen;
			$scope.blue = outBlue;
		}
	});
	setAlphabet();
	function setAlphabet()
{
	var divy = "";
	var element;
	for (i = 0; i < 255; i++) 
	{
		element = "eleR" + i;
		divy = divy + '<div class="color" id="'+element+'"  style="background-color:rgb('+i+',0,0);" ng-mouseover="setColor('+i+',0,0);"></div>';
	}
	document.getElementById("redWrapper").innerHTML = divy;
	divy = "";
	element = "";
	for (i = 0; i < 255; i++) 
	{
		element = "eleG" + i;
		divy = divy + '<div class="color" id="'+element+'"  style="background-color:rgb(0,'+i+',0);" ng-mouseover="setColor(0,'+i+',0);"></div>';
	}
	document.getElementById("greenWrapper").innerHTML = divy;
	divy = "";
	element = "";
	for (i = 0; i < 255; i++) 
	{
		element = "eleB" + i;
		divy = divy + '<div class="color" id="'+element+'"  style="background-color:rgb(0,0,'+i+');" ng-mouseover="setColor(0,0,'+i+');"></div>';
	}
	document.getElementById("blueWrapper").innerHTML = divy;
	
}